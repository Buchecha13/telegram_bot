<?php

namespace App\Console\Commands;

use App\Models\QuestionAnswer;
use App\Services\TelegramBot\TelegramBotServices;
use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartTelegramBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:bot:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $telegram;

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TelegramBotServices $telegram)
    {
        while (true) {
            $telegram->boot();
            sleep(3);
        }
    }
}
