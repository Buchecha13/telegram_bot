<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'is_correct'
    ];

    public function isCorrect($answerId) : string
    {
        $answer = QuestionAnswer::query()->where('id', $answerId)->first();
        if ($answer->is_correct) {
            return true;
        }
        return false;
    }

    public function getResultText($answerId)
    {
        if ($this->isCorrect($answerId)) {
            return 'Правильный ответ';
        } else {
            return 'Неправильный ответ';
        }
    }
}
