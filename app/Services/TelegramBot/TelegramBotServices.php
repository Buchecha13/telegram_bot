<?php


namespace App\Services\TelegramBot;

use App\Models\Question;
use App\Models\QuestionAnswer;
use App\Models\User;
use Telegram\Bot\Answers\Answerable;
use Telegram\Bot\Api as TelegramBot;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Methods\Message;

class TelegramBotServices
{
    private QuestionAnswer $answer;


    public function __construct(QuestionAnswer $answer)
    {
        $this->answer = $answer;
    }


    /**
     */
    public function boot()
    {
        $this->eventHandle();
    }

    /**
     * Метод определяет обрабатывает входящие данные от пользователя
     */
    protected function eventHandle()
    {
        $event = Telegram::commandsHandler();

        if (isset($event[0]['callback_query']['data'])) {
            $userId = $event[0]['callback_query']['message']['chat']['id'];
            $answerId = $event[0]['callback_query']['data'];
            $this->sendResultMessage($userId, $answerId);
            $this->setScore($userId, $answerId);

            sleep(1.5);

            $this->sendNexRiddle($userId);
        }
    }

    /**
     * Метод отправки сообщения с результатом ответа на загадку
     * @param $userId
     * @param $answerId
     */
    protected function sendResultMessage($userId, $answerId)
    {
        Telegram::sendMessage(['chat_id' => $userId, 'text' => "<b><i> {$this->answer->getResultText($answerId)} </i></b>", 'parse_mode' => 'html']);
    }

    /**
     * Метод отправляет следующую загадку пользователю
     *
     * @param $userId
     */
    protected function sendNexRiddle($userId)
    {
        $question = $this->getQuestion();
        $questionId = $question->id;

        Telegram::sendMessage(['chat_id' => $userId, 'text' => $this->getQuestion()->text, 'reply_markup' => $this->getAnswers($questionId)]);
    }


    protected function setScore($userId, $answerId)
    {
        if ($this->answer->isCorrect($answerId)) {
            $user = User::query()->where('telegram_id', $userId)->first();
            $user->setScore();
            $user->save();
        }
    }

    public function getQuestion()
    {
        $question = Question::query()->orderByRaw("RAND()")->first();
        return $question;
    }

    public function getAnswers($questionId)
    {
        $answerButtons = new InlineKeyboardButtonServices;
        return $answerButtons->getAnswerButton($questionId);
    }
}
