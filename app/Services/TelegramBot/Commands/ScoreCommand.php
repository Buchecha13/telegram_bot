<?php


namespace App\Services\TelegramBot\Commands;


use App\Models\User;
use Telegram\Bot\Commands\Command;

/**
 * Class HelpCommand.
 */
class ScoreCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'score';

    /**
     * @var string Command Description
     */
    protected $description = 'Отображает твой счет';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $telegram_id = $this->getUpdate()['message']['from']['id'];
        $user = User::query()->where('telegram_id', $telegram_id)->first();

        $userScore = $user->score;

        if ($user) {
            $this->replyWithMessage(['text' => "Твой счет отгаданных загадок: $userScore "]);
        } else {
            $this->replyWithMessage(['text' => "Сначала отгадай загадку ( /start )"]);
        }
    }
}

