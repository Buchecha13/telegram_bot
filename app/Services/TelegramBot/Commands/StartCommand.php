<?php


namespace App\Services\TelegramBot\Commands;

use App\Models\Question;
use App\Models\QuestionAnswer;
use App\Services\TelegramBot\InlineKeyboardButtonServices;
use App\Services\TelegramBot\TelegramBotServices;
use App\Services\TelegramBot\UserService;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

/**
 * Class HelpCommand.
 */
class StartCommand extends Command
{

    protected $buttons;
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * @var string Command Description
     */
    protected $description = 'Start this bot';

    /**
     * StartCommand constructor.
     * @param $buttons
     */


    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $telegram_id = $this->update->getChat()->id;

        $telegramBotService = new TelegramBotServices(new QuestionAnswer());
        $question = $telegramBotService->getQuestion();

        $questionText = $question->text;
        $questionId = $question->id;

        $answerButtons = $telegramBotService->getAnswers($questionId);

        $user = new UserService;
        $user->registerTelegramUser($telegram_id);

        $this->replyWithMessage(['text' => $questionText, 'reply_markup' => $answerButtons]);
    }
}
