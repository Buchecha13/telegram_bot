<?php


namespace App\Services\TelegramBot;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserService
{
    public function registerTelegramUser($telegramId): User
    {
        return User::firstOrCreate([
            'telegram_id' => $telegramId
        ]);
    }
}
