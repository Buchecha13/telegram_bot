<?php


namespace App\Services\TelegramBot;


use App\Models\QuestionAnswer;
use Telegram\Bot\Keyboard\Keyboard;

class InlineKeyboardButtonServices
{
    public function getAnswerButton($questionId)
    {
        $answersBtn = [
            'inline_keyboard' => [

            ]
        ];
        $answers = QuestionAnswer::query()->where('question_id', $questionId)->get();

        foreach ($answers as $answer) {
            $answersBtn['inline_keyboard'][][] =
                Keyboard::inlineButton(['text' => $answer->text, 'callback_data' => $answer->id]);
        }

        return json_encode($answersBtn, true);
    }
}
