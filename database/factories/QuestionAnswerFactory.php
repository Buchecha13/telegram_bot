<?php

namespace Database\Factories;

use App\Models\QuestionAnswer;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionAnswerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QuestionAnswer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'question_id' => rand(1, 5),
            'text' => $this->faker->text(10),
            'is_correct' => 0,
        ];
    }

    public function suspended()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_correct' => 1,
            ];
        });
    }
}
