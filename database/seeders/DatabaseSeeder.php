<?php

namespace Database\Seeders;

use App\Models\Question;
use App\Models\QuestionAnswer;

use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Question::factory()->count(5)->create();
        QuestionAnswer::factory()->count(5)->suspended()->state(new Sequence(
            ['question_id' => 1],
            ['question_id' => 2],
            ['question_id' => 3],
            ['question_id' => 4],
            ['question_id' => 5],
        ))->create();
        QuestionAnswer::factory()->count(15)->state(new Sequence(
            ['question_id' => 1],
            ['question_id' => 2],
            ['question_id' => 3],
            ['question_id' => 4],
            ['question_id' => 5],
        ))->create();
    }
}
